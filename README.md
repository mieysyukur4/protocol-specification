# Abstract

CashID is an open protocol that allows secure authentication based on the public key cryptography infrastructure that is currently present in the Bitcoin Cash ecosystem. Each user can prove to a service provider that they control a specific Bitcoin Cash address by signing a challenge request, as well as provide optional metadata.



# Motivation

As a means of authentication, passwords have inherent security and UX issues. These issues are often mitigated by means of password managers, permanent sessions and single-signon features. CashID is designed to replace password authentication, thus solving the security and UX issues it has, instead of simply mitigating them.

Secure authentication with optional metadata is useful, for example to...

* Register with a service as a 1-step process.
* Register with a service automatically as part of a payment.
* Authenticate to a service to login without a password.
* Authenticate to a service to act as 2FA complementing existing passwords.
* Provide access control to locks based on payment.
* Support transient user profiles.


# Specification

## Introduction

When a user needs to access a physical or digital restricted area they are given a **Challenge request** by the service provider. The identity manager presents the request to the user and allows them to choose a suitable keypair to represent their identity.

If metadata was requested the identity manager provides the user the option to select which data to use for each metadata field, as well as the option to not supply information for any given field. If the user denies sharing of metadata for a field marked as required, the identity manager aborts the request.

When the user has chosen an identity, the identity manager signs the challenge request, adds the metadata that was approved by the user and sends a **Challenge response** back to the service provider.

The service provider validates the response and returns a **Confirmation status**. If the confirmation contains a status message it is shown to the user.

The service provider can now use the address as a user identifier and perform the requested action.



## Challenge request

The request consists of an **Intent**, **Domain**, **Path** and set of **Parameters**.

```
cashid:domain.tld/cashid?a=action&d=data&r=required&o=optional&x=nonce
```

**Part** | **Example** | **Description**
--- | --- | ---
Intent | cashid: | Protocol identifier
Domain | domain.tld | Fully qualified domain name
Path | /cashid | Path to a request manager
Parameters | ?a=action<br/>&d=data<br/>&r=required<br/>&o=optional<br />&x=nonce | Various parameters



#### Parameters

Every request must have a **Nonce** parameter and may also have an **Action**, **Data**, **Required** and **Optional** parameter.

**Letter** | **Name** | **Description**
--- | --- | ---
a | Action | Name of the action the user authenticates to perform
d | Data | Data relevant to the requested action
r | Required | List of metadata that the action needs to function
o | Optional | List of metadata that the action can use but still works without
x | Nonce | Random data unique for this request



#### Nonce

The **Nonce** parameter acts as a replay-protection mechanism, by making each request challenge and response unique. Each **Nonce** can only be used a single time during its lifespan and should expire if left unused for a significant amount of time.

The service provider should not process requests that it has not issued nonces for, except for **User actions** that have a valid and recent **Timestamp** according to [ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) as their nonce value.


#### Action and Data

The optional **Action** parameter is used to describe the action that the user authenticates to perform. When a **Data** parameter is present, the **Action** also determines how to interpret it. When omitted, the **Action** is assumed to have the default value of "**auth**" which authenticates with an optional **Data** message.

Identity managers may implement any **predefined** and **custom** actions, but must implement the default **auth** action.

For a list of **Service** and **User** actions, see the <a href='#action-types'>Actions resource</a>.



#### Required and Optional Metadata

The **Required** and **Optional** parameters allow the service to request personal information from the user. In order to do this in the least amount of space, the information is shorthanded by a letter and a string of numbers representing various pieces of personal information. For **Optional** metadata, the numbers after a letter can be omitted to request all fields in that category. The field numbers in each category has to be listed in sorted order, starting with the lowest number. If the same information is requested in both required and optional scope, the information is required.

For a list of **Identity**, **Position** and **Contact** metadata fields, see the <a href='#metadata-types'>Metadata resource</a>.



## Challenge Response

When the identity manager is ready to submit a **Challenge response** it forms a **Response URL** by appending the **Domain** and **Path** components to a **https://** scheme identifier.

```
https://domain.tld/cashid
  ^        ^          ^
cashid: domain.tld/cashid?a=action&d=data&r=required&o=optional&x=nonce
```

It then sends the **Response data** as a **JSON** encoded object with the following members:

**Member** | **Data type** | **Description**
--- | --- | ---
request | String | The challenge request URI
address | String | Address used to sign the request
signature | String | Signature for the request
metadata | Object | Requested metadata fields


#### Address

The **Address** field should be a valid Bitcoin Cash address in the [CashAddr](https://github.com/bitcoincashorg/bitcoincash.org/blob/master/spec/cashaddr.md) format.

#### Signature

The **Signature** must comply with the "\x18Bitcoin Signed Message:\n#{message.size.chr}#{message}" format and should sign the full challenge request string.

#### Metadata

Metadata is stored as a JSON object with property names matching the requested field names. Fields that have multiple values is stored as a JSON object with property names matching their identifier and values matching the field description.



## Confirmation status

The service provider should supply a **Confirmation status** containing a **Status code** and optionally a **Status message**.

**Member** | **Data type** | **Description**
--- | --- | ---
status | Integer | Code that identifies the request status.
message | String | Additional information to show the user.

<br />

For a list of **Status codes**, see the <a href='#status-codes'>Status codes resource</a>.

<br />

-----

<br />

# Resources

## Action types

**Service actions** can only be requested by the service provider.

**Action** | **Description** | **Data**
--- | --- | ---
auth | Identifies the user to the service provider | Optional message to display to the user
login | Grants the user access to digital services | Session identification ID used to link the login request with an active anonymous session
sign | Asks the user to cryptographically sign a message | Message text that the identity is requested to sign
register | Registers the user with a service, for example a newsletter or drawing of a lottery. | Description of the service the user signs up to
ticket | Grants an identity future access to a resource, for example a ride in a rollercoaster or a seat at a conference | Description of the resource and ticket validity
claimtx? | Asks the user to prove ownership of any address used in a transaction | Transaction identifier
claimaddr? | Asks the user to prove ownership of a specific address | Address identifier

<br />

**User actions** can be sent unsolicited to the service provider.

**Value** | **Description**
--- | ---
delete | Requests the service provider to delete this identity and related metadata
logout | Requests the service provider to close all active sessions for this identity
revoke | Informs the service provider that this identity has been compromised
update | Informs the service provider about changes to identity, position or contact metadata



## Metadata types

**Identity** is represented by the letter **i** followed by a list of numbers corresponding to the following table.

**Number** | **Name** | **Description** | **Examples**
--- | --- | --- | ---
1 | Name | The first or given name of the person | John <br /> Jane
2 | Family | The last name or family name of the person | Doe <br > Simpsons
3 | Nickname | A nickname or username for the person | SweetMafia55
4 | Age | The number of years the person has lived | 31
5 | Gender | The sex of the person, usually "Male" or "Female" | Male <br /> Robot
6 | Birthdate | The date of birth as an ISO-8601 date. | 1969-04-01
8 | Picture | URL to a profile picture, or Base64 encoded image | https://www.yours.org/gravatar/satoshidoodles/180 <br/> data:image/jpeg;base64,/9j/4AAQSkZJRgAB...igAA96KKKAP/Z
9 | National | National identification numbers, such as passport, drivers license and citizenship numbers. | 19840801-1221

<br />

**Position** is represented by the letter **p** followed by a list of numbers corresponding to the following table.

**Number** | **Name** | **Description** | **Examples**
--- | --- | --- | ---
1 | Country | Name of the nation | Norway
2 | State | Name of the state or province | Westshire
3 | City | Name of the city | New York
4 | Street name | Name of the street, without the street number | Elm street
5 | Street number | The street number | 41
6 | Residence | Building or apartment number that uniquely reference a residence | LGH1102
9 | Coordinate | Geographical position as specificed in [RFC5870](https://tools.ietf.org/html/rfc5870) | geo:13.4125,103.8667

<br />

**Contact information** is represented by the letter **c** followed by a list of numbers corresponding to the following table.

**Number** | **Name** | **Description** | **Examples**
--- | --- | --- | ---
1 | Email | Email address | john@doe.net
2 | Instant | Instant Messenger protocol handle | { 'icq': 148264 } <br /> { 'jabber': 'testusers@networks.com' }
3 | Social | Social media service handle or URL | { 'twitter': '@bitcoin' } <br /> { 'facebook': 'facebook.com/johndoe' }
4 | Phone | Phone number<br/>*In [ITU-T E.164](https://en.wikipedia.org/wiki/E.164) format* | +1 555 1234567
5 | Postal | Postal label for a physical address to which <br /> a service can send letters and packages | Jonathan Lionheart <br /> Elm street 41 <br/> 409 33 Gothenburg <br /> Sweden



## Status Codes

**Status codes** are used in the **Request Confirmation**.

**Code** | **Error message** | **Description**
--- |  --- | ---
  0 | Authentication successful | The request was completed without errors.
100 | Request broken | The request could not be parsed.
111 | Request missing intent | The intent could not be found in the request.
112 | Request missing domain | The domain could not be found in the request.
113 | Request missing nonce | The nonce could not be found in the request.
121 | Request malformed intent | The intent does not match the specification.
122 | Request malformed domain | The domain is not a fully qualified domain name.
131 | Request invalid domain | The domain does not match this service.
132 | Request invalid nonce | The nonce does not match this service.
141 | Request altered | The request has been changed.
142 | Request expired | The request was not used in time.
143 | Request consumed | The request has already been used.
200 | Response broken | The response could not be parsed.
211 | Response missing request | The request could not be found in the response.
212 | Response missing address | The address could not be found in the response.
213 | Response missing signature | The signature could not be found in the response.
214 | Response missing metadata | The metadata could not be found in the response.
221 | Response malformed address | The address format is incorrect.
222 | Response malformed signature | The signature format is incorrect.
223 | Response malformed metadata | The metadata format is incorrect.
231 | Response invalid method | The transmission method does not match specification.
232 | Response invalid address | The address is not valid.
233 | Response invalid signature | The signature is not valid.
234 | Response invalid metadata | The metadata is not valid.
300 | Service broken | The action+data could not be parsed.
311 | Service address denied | The service is not allowed for this address.
312 | Service address revoked | The service has revoked this address.
321 | Service action denied | The service action was denied.
322 | Service action unavailable | The service action is unavailable.
323 | Service action not implemented | The service action is not implemented.
331 | Service internal error | The service encountered an internal error.


#### Previous Status Codes

In a previous version of this document the status codes were different, but due to a vast number of issues encountered during initial implementations they are being scheduled to be removed from the specification.

**Code** | **Error message** | **Description**
--- |  --- | ---
 0 | Authentication successful | The request was completed without errors.
 1 | Malformed response | The response data could not be parsed.
 2 | Malformed request  | The request URI in the response could not be parsed.
 3 | Malformed address  | The address in the response could not be parsed.
 4 | Malformed signature  | The signature in the response could not be parsed.
 5 | Malformed metadata  | The request metadata field could not be parsed.
 6 | Invalid nonce | The nonce was not issued by the service.
 7 | Expired nonce | The nonce was not used in time and is no longer valid.
 8 | Consumed nonce | The nonce has already been used and cannot be used again.
 9 | Signature verification failed | The signature, address and message verification failed.
10 | Identity denied | The address used is not allowed at this service.
11 | Identity revoked | The address used has been marked as compromised and should not be used.
12 | Required metadata is missing | Metadata field marked as required was not supplied.
13 | Required metadata is malformed | Metadata field marked as required could not be parsed.
14 | Action not implemented | The requested service action is not supported.
15 | Action currently unavailable | The requested service action is temporarily unavaible.
16 | Action denied | The address is not allowed to do this service action.



## Examples

### Minimal authentication request

The smallest possible request only authenticates a user.

```
cashid:domain.tld/path?x=2671757324
```

The JSON encoded response for a user with address **qrprejjynve6e0qaecylv5m0k8a48acyvs5759l8kl** will be sent to **https://domain.tld/path**:

```javascript
{
    'request': 'cashid:domain.tld/path?x=2671757324',
    'address': 'qrprejjynve6e0qaecylv5m0k8a48acyvs5759l8kl',
    'signature': 'HyGJKQYaMk9aZ38Q9IJBCFi6a0q+QueIICEGCNsDJZaBft6SUMRNy5FGD0Rb2XIQ51Arff408AnrVxtg3tiWs4g='
}
```

The service provider will validate the request and return an **Authentication successful** status message:

```javascript
{
    'status': 0
}
```

### Logging in to a website

The **login** action for session **15366-4133-6141-9638** at **cashtalk.org** can use **Nickname** and **Picture** to update the user profile.
```
cashid:cashtalk.org/cashid?a=login&d=15366-4133-6141-9638&o=i38&x=13534642624
```

The JSON encoded response for a user with address **qz0tyktxwyxdhx0zt4essf5ngfqwv3z6nyrq45n3x7** with an updated **Picture** will be sent to **https://cashtalk.org/cashid**:

```javascript
{
    'request': 'cashid:cashtalk.org/cashid?a=login&d=15366-4133-6141-9638&o=i38&x=13534642624',
    'address': 'qz0tyktxwyxdhx0zt4essf5ngfqwv3z6nyrq45n3x7',
    'signature': 'HwSDACaveIdcdKxRJHQNWEV+wWK57XU4WfiRBusiR6PeaiWl8m/1VgCH5SYF3PO5YozJtrw5SGdCJo28SD2scbw=',
    'metadata':
    {
        'picture': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAAB[...]'
    }
}
```

### Signing up for a newsletter

The **register** action for the **newsletter** at **bitcoin.com** requires **Name**, **Family**, **Country** and **Email** to work, and will use **Picture**, **Age**, **Gender** and **City** if provided.
```
cashid:bitcoin.com/api/cashid?a=register&d=newsletter&r=i12l1c1&o=i458l3&x=95261230581
```

The JSON encoded response for a user with address **qqagsast3fq0g43wnrnweefjsk28pmyvwg7t0jqgg4** and no optional information will be sent to **https://bitcoin.com/api/cashid**:

```javascript
{
    'request': 'cashid:bitcoin.com/api/cashid?a=register&d=newsletter&r=i12l1c1&o=i458l3&x=95261230581',
    'address': 'qqagsast3fq0g43wnrnweefjsk28pmyvwg7t0jqgg4',
    'signature': 'IKjtNWdIp+tofJQrhxBrq91jLwdmOVNlMhfnKRiaC2t2C7vqsHRoUA+BkdgjnOqX6hv4ZdeG9ZpB6dMh/sXJg/0=',
    'metadata':
    {
        'name': 'John',
        'family': 'Doe',
        'country': 'United States',
        'email': 'john@does.net'
    }
}
```

### Account removal

The user initiated request to **delete** their user data from **sensitive.cash**
```
cashid:sensitive.cash/api/cashid?a=delete&x=20180929T063418Z
```

The JSON encoded response for a user with address **qzvelmkfzvq8gw0d4fvmf904ghefq66keq68qwupsv** will be sent to **https://sensitive.cash/api/cashid**:

```javascript
{
    'request': 'cashid:sensitive.cash/api/cashid?a=delete&x=20180929T063418Z',
    'address': 'qzvelmkfzvq8gw0d4fvmf904ghefq66keq68qwupsv',
    'signature': 'IDwIyQCsmFKwWWibwtxVqppt+KCDBgTKy4IN8+rL+8a9XtGN/AAl/koKPKnIQOr2/nlzOW9XaxtWP96298XkiJE='
}
```

### Transient user profiles

The **login** action for session **a7fbb9341ce3ae21** at **democratictools.net** (which as a security measure does not store user profile information) will only work if the user provides a **nickname** and **email**, and will give a better customized user experience if the user provides **Country**, **Age**, **Gender** and a **Postal label**.

```
cashid:democratictools.net/fn/auth?a=login&d=a7fbb9341ce3ae21&r=i3c1&o=i45p1c7&x=4295861935820
```

The JSON encoded response for a user with address **qqndur5yga4uej625xvatt03d0ryzd4u9sdj99f2mw** that optionally shares **Country** and **Age** will be sent to **https://democratictools.net/fn/auth**:

```javascript
{
    'request': 'cashid:democratictools.net/fn/auth?a=login&d=a7fbb9341ce3ae21&r=i3c1&o=i45p1c7&x=4295861935820',
    'address': 'qqndur5yga4uej625xvatt03d0ryzd4u9sdj99f2mw',
    'signature': 'IN+npgZWwi+wDAgG4Za4goSws9jNcZG5EK5EPKuG/b/TMtzY67kU8olR26tAjDomQvf5KJif2Fc1nTdN8v10dpc=',
    'metadata':
    {
        'nickname': 'Activ1337',
        'email': 'activ1337@gmail.com',
        'country': 'Norway',
        'age': 31
    }
}
```


## Implementation support

In order to reduce implementation costs and increase implementation conformity, this specification supplies extra material that can be helpful to developers. 

### Regular expressions

*The expressions listed here is shown with whitespace and linebreaks for clarity and ease of updating and should be removed before use.*

To parse a **Challenge request** into its parts
```regexp
/
    (?P<intent>cashid:)
    (?:[\/]{2})?
    (?P<domain>[^\/]+)
    (?P<path>\/[^\?]+)
    (?P<parameters>\?.+)
/
```

To parse the **Parameters** into their parts
```regexp
/
    (?(?:[\?\&]a=)(?P<action>[^\&]+))?
    (?(?:[\?\&]d=)(?P<data>[^\&]+))?
    (?(?:[\?\&]r=)(?P<required>[^\&]+))?
    (?(?:[\?\&]o=)(?P<optional>[^\&]+))?
    (?(?:[\?\&]x=)(?P<nonce>[^\&]+))?
/
```

To parse the **Metadata** fields into their parts
```regexp
/
    (i
        (?P<identity>(?![1-9]+))?
        (?P<name>1)?
        (?P<family>2)?
        (?P<nickname>3)?
        (?P<age>4)?
        (?P<gender>5)?
        (?P<birthdate>6)?
        (?P<picture>8)?
        (?P<national>9)?
    )?
    (p
        (?P<position>(?![1-9]+))?
        (?P<country>1)?
        (?P<state>2)?
        (?P<city>3)?
        (?P<streetname>4)?
        (?P<streetnumber>5)?
        (?P<residence>6)?
        (?P<coordinate>9)?
    )?
    (c
        (?P<contact>(?![1-9]+))?
        (?P<email>1)?
        (?P<instant>2)?
        (?P<social>3)?
        (?P<phone>4)?
        (?P<postal>5)?
    )?
/
```



### Libraries

**CashID** specific libraries:

* reference libraries (https://gitlab.com/cashid/libraries)
* react-cashid (https://github.com/paOol/react-cashid)
* cashid (https://github.com/paOol/CashID)

**Bitcoin Cash** generic libraries:

* bitcoincashjs

## Links and Refences

### BitID

The CashID specification was built on and inspired by the BitID draft and metadata documents:

- Specification: https://github.com/bitid/bitid/blob/master/BIP_draft.md
- Metadata: https://github.com/bitid/bitid/blob/master/bitid_metadata.md



## TODO

These items is an ad-hoc todo list of things to do make CashID useful and widespread, and does not necessarily belong in the specification document.

* code examples
* a website with interface/workflow example, similar to BitID
* interface guidelines (color-coding privacy impact, etc.)
* Oath1/2 server that authenticates with CashID, as a compatibiltiy bridge.
* Introduction video in the style of we-use-coins
* Look at alternatives to using 'application/json' headers with the encoded response in the body of the request, and then clarify in more detail what should be used.
* Consider using CashID for a legal canary notification, and/or usecase example showcasing how CashID simplified the process.